# PROJET QT : Jeu Takuzu
Ludovic Thai \
Benjamin Vignaux

## Fonctionnalités suggérées
### Implémentées
- Choix de la taille et du niveau de difficulté
- Indication des lignes/colonnes correctes
- Chronomètre indiquant le temps de jeu
- Mise en évidence des erreurs : plus de 3 cases contigues de même couleurs 
  et lignes/colonnes incorrectes
- Indication chiffrée du nombre de cases de chaque couleur sur chaque ligne/colonne
- Décompte du nombre de retours en arrière effectué par le joueur

### Non implémentées
- Assistance au joueur

## Particularités innovantes de l'interface
- Règles du jeu et mode d'emploi spécique à ce jeu
- Possibilité d'afficher les règles du jeu tout en jouant 
- Fonction "redo" après un "undo"
- Fonction de "reset" du jeux mettant le puzzle et le chronomètre à zéro
- Fonction de pause mettant en pause le chronomètre et cachant le jeu
- A la fin d'un puzzle : possibilité de faire le suivant, le recommencer ou 
  retourner au menu
- Mode aléatoire : choix d'une difficulté, taille et numéro de puzzle aléatoire
- Possibilité de  changer les symboles: rond, carré,  binaire, tic tac
  toe (bar de menu)
- Raccourcis clavier pour "redo", "do", quitter, "reset"
- Fonction de retour au menu depuis la fenêtre de jeu

## Choix effectués concernant l'érgonomie
- 3 fenêtres: le menu, le jeu et les règles du jeu
- Menu évolutif : choix de la taille, de la difficulté puis du puzzle; puis 
  apparition du jeu
- Clique (droit et gauche) pour changer les symboles sur les cases 
- Possibilité d'utilisation de raccourcis clavier 

## Choix de modélisation
- Utilisation d'un patron d'architecture MVP pour le jeu: la vue (ViewGame, ViewMenu, 
  ViewGameRules) et le modèle (TakuzuModel, Puzzle) ne communiquent uniquement 
  à travers le Presenter. 
- Utilisation du patron de conception COMMANDE pour la fonction de "undo/redo"

## Répartition du travail
Le temps de travail ainsi que la quantité a été relativement égale. \

Au  début du  projet, Benjamin  Vignaux a  plus travaillé  sur la  vue
tandis que Ludovic Thai s'est concentré  sur le modèle afin de limiter
les conflits  en travaillant  sur des fichiers  différents. Cependant,
après  une certaine  avancée dans  le projet,  chaque a  travaillé sur
toutes les parties du projets (vue, présentation et modèle).

## Remarques
1. Il est recommandé d'utilisé QtCreator pour créer l'exécutable.
Le jeux a été développé et testé sous Linux (Ubuntu 18.04 LTS et Kubuntu
18.04 LTS) au sein de l'IDE QtCreator.
Il est possible que sous d'autres système d'exploitation, les polices de
caractères ne correspondent pas ou n'étant pas installées pouvant ainsi
causer problèmes d'affichages.\
Dans la mesure du possible, ce sont les polices de caractères les plus courantes
qui ont été utilisées. 

2. Pour que le menu s'affiche correctement, il faut s'assurer que ce soit la page
*page* qui soit selectionnée dans le *stackedWidget* dans QtCreatior.
