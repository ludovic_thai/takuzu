#include "puzzle.h"

Puzzle::Puzzle(Difficulty difficulty, int size, int num, std::string puzzle)
{
    _difficulty = difficulty;
    _size = size;
    _num = num;
    _initialPuzzle = puzzle;

    _currentPuzzle = new int*[size];
    _validationPuzzle = new int*[_size];
    _validationPuzzleColumn = new int*[_size];
    _validationPuzzleRow = new int*[_size];
    _rowInformations = new int*[_size];
    _columnInformations = new int*[_size];

    for (int i = 0; i < size; i++) {
        _currentPuzzle[i] = new int[_size];
        _validationPuzzle[i] = new int[_size];
        _validationPuzzleColumn[i] = new int[_size];
        _validationPuzzleRow[i] = new int[_size];
    }

    for (int i=0; i<_size; i++) {
        _rowInformations[i] = new int[3];
        _columnInformations[i] = new int[3];
    }

    createCurrentPuzzle();
    initializeInformationsArray();
    _validPuzzle = checkPuzzle();
}

void Puzzle::createCurrentPuzzle() {
    for (int i = 0; i < _size*_size; i++) {
        if (_initialPuzzle.at(i) == 'B') {
            _currentPuzzle[i/_size][i%_size] = BLACK;
        } else if (_initialPuzzle.at(i) == 'W') {
            _currentPuzzle[i/_size][i%_size] = WHITE;
        } else {
            _currentPuzzle[i/_size][i%_size] = EMPTY;
        }
    }
}

void Puzzle::initializeInformationsArray() {
    for (int i=0; i<_size; i++) {
        for (int j=0; j<3; j++) {
            _rowInformations[i][j] = 0;
            _columnInformations[i][j] = 0;
        }
    }
}

int** Puzzle::getCurrentPuzzle() {
    return _currentPuzzle;
}

void Puzzle::updateCell(int x, int y, Value value) {
    _currentPuzzle[x][y] = value;
    _validPuzzle = checkPuzzle();
}

bool Puzzle::compareTwoRows(int firstRow, int secondRow) {
    for (int i=0; i<_size; i++) {
        if (_currentPuzzle[firstRow][i] != _currentPuzzle[secondRow][i]) {
            return false;
        } else if (_currentPuzzle[firstRow][i] == EMPTY ||
                   _currentPuzzle[firstRow][i] == EMPTY) {
            return false;
        }
    }
    return true;
}

bool Puzzle::compareTwoColumns(int firstColumn, int secondColumn) {
    for (int i=0; i<_size; i++) {
        if (_currentPuzzle[i][firstColumn] != _currentPuzzle[i][secondColumn]) {
            return false;
        } else if (_currentPuzzle[i][firstColumn] == EMPTY ||
                   _currentPuzzle[i][secondColumn] == EMPTY) {
            return false;
        }
    }
    return true;
}

Puzzle::ValidationValue Puzzle::checkOneRow(int row) {
    Value currentValue = EMPTY;
    int countCurrentValue = 0;
    ValidationValue valid = CORRECT;
    int countBack = 0;
    int countWhite = 0;

    for (int j=0; j<_size; j++) {
        switch (_currentPuzzle[row][j]) {
        case (EMPTY) :
            if (valid != INCORRECT) {
                valid = NONE;
            }
            _validationPuzzleRow[row][j] = NONE;
            if (currentValue != EMPTY && countCurrentValue > 2) {
                for (int k=j-1; k>j-countCurrentValue-1; k--) {
                    _validationPuzzleRow[row][k] = INCORRECT;
                }
            }
            currentValue = EMPTY;
            countCurrentValue = 0;
            break;
        default :
            _validationPuzzleRow[row][j] = NONE;
            if (_currentPuzzle[row][j] == BLACK) {
                countBack++;
            } else {
                countWhite++;
            }
            if (currentValue == _currentPuzzle[row][j]) {
                countCurrentValue++;
            } else {
                currentValue = (Value)_currentPuzzle[row][j];
                if (countCurrentValue > 2) {
                    valid = INCORRECT;
                    for (int k=j-1; k>j-countCurrentValue-1; k--) {
                        _validationPuzzleRow[row][k] = INCORRECT;
                    }
                }
                countCurrentValue = 1;
            }
            break;
        }
        if (countCurrentValue > 2) {
            valid = INCORRECT;
            for (int k=_size-1; k>_size-countCurrentValue-1; k--) {
                _validationPuzzleRow[row][k] = INCORRECT;
            }
        }
    }
    _rowInformations[row][1] = countBack;
    _rowInformations[row][2] = countWhite;

    // Complete row respecting the rule "max two in a row"
    if (valid == CORRECT) {
        if (countBack == countWhite) {
            _rowInformations[row][0] = 1;
        } else {
            for (int i=0; i<_size; i++) {
                _validationPuzzleRow[row][i] = INCORRECT;
                valid = INCORRECT;
            }
        }
    }
    return valid;
}

Puzzle::ValidationValue Puzzle::checkOneColumn(int column) {
    Value currentValue = EMPTY;
    int countCurrentValue = 0;
    ValidationValue valid = CORRECT;
    int countBlack =0;
    int countWhite =0;

    for (int i=0; i<_size; i++) {
        switch (_currentPuzzle[i][column]) {
        case (EMPTY) :
            if (valid != INCORRECT) {
                valid = NONE;
            }
            _validationPuzzleColumn[i][column] = NONE;
            if (currentValue != EMPTY && countCurrentValue > 2) {
                for (int k=i-1; k>i-countCurrentValue-1; k--) {
                    _validationPuzzleColumn[k][column] = INCORRECT;
                }
            }
            currentValue = EMPTY;
            countCurrentValue = 0;
            break;
        default :
            _validationPuzzleColumn[i][column] = NONE;
            if (_currentPuzzle[i][column] == BLACK) {
                countBlack++;
            } else {
                countWhite++;
            }
            if (currentValue == _currentPuzzle[i][column]) {
                countCurrentValue++;
            } else {
                currentValue = (Value)_currentPuzzle[i][column];
                if (countCurrentValue > 2) {
                    valid = INCORRECT;
                    for (int k=i-1; k>i-countCurrentValue-1; k--) {
                        _validationPuzzleColumn[k][column] = INCORRECT;
                    }
                }
                countCurrentValue = 1;
            }
            break;
        }
        if (countCurrentValue > 2) {
            valid = INCORRECT;
            for (int k=_size-1; k>_size-countCurrentValue-1; k--) {
                _validationPuzzleColumn[k][column] = INCORRECT;
            }
        }
    }

    _columnInformations[column][1] = countBlack;
    _columnInformations[column][2] = countWhite;
    // Complete column respecting the rule "max two in a row"
    if (valid == CORRECT) {
        if (countBlack == countWhite) {
            _columnInformations[column][0] = 1;
        } else {
            for (int i=0; i<_size; i++) {
                _validationPuzzleColumn[i][column] = INCORRECT;
                valid = INCORRECT;
            }
        }
    }
    return valid;
}


bool Puzzle::checkAllRowsDifferent() {
    bool valid = true;
    for (int i=0; i<_size; i++) {
        for (int j=i+1; j<_size; j ++) {
            if (compareTwoRows(i, j)) {
                valid = false;
                for (int k=0; k<_size ; k++) {
                    _validationPuzzleRow[i][k] = INCORRECT;
                    _validationPuzzleRow[j][k] = INCORRECT;
                }
            }
        }
    }
    return valid;
}

bool Puzzle::checkAllColumnsDifferent() {
    bool valid = true;
    for (int i=0; i<_size; i++) {
        for (int j=i+1; j<_size; j ++) {
            if (compareTwoColumns(i, j)) {
                valid = false;
                for (int k=0; k<_size ; k++) {
                    _validationPuzzleColumn[k][i] = INCORRECT;
                    _validationPuzzleColumn[k][j] = INCORRECT;
                }
            }
        }
    }
    return valid;
}

bool Puzzle::checkPuzzle() {
    bool valid = true;

    initializeInformationsArray();
    // check rows and columns separtly
    for (int k =0; k<_size; k++) {
        if (checkOneRow(k)!=CORRECT) {
            valid = false;
        }
        if (checkOneColumn(k)!=CORRECT) {
            valid = false;
        }
    }

    // compares rows and columns between them
    if (!checkAllRowsDifferent()) {
            valid = false;
    }
    if (!checkAllColumnsDifferent()) {
            valid = false;
    }

    // Merge data from rows and columns
    for (int i=0; i<_size; i++) {
        for (int j=0; j<_size; j++) {
            if (_validationPuzzleRow[i][j] == NONE
                    && _validationPuzzleColumn[i][j] == NONE) {
                _validationPuzzle[i][j] = NONE;
            } else {
                _validationPuzzle[i][j] = INCORRECT;
                _rowInformations[i][0] = 0;
                _columnInformations[j][0] = 0;
            }
        }
    }

    // Put green cell
    for (int i=0; i<_size; i++) {
        if (_rowInformations[i][0]) {
            for (int k=0; k<_size; k++) {
                _validationPuzzle[i][k] = CORRECT;
            }
        }
        if (_columnInformations[i][0]) {
            for (int k=0; k<_size; k++) {
                _validationPuzzle[k][i] = CORRECT;
            }
        }
    }

    return valid;
}

int ** Puzzle::getValidationPuzzle() {
    return _validationPuzzle;
}

int Puzzle::getSize() {
    return _size;
}

int Puzzle::getBlackOnRow(int row) {
    return _rowInformations[row][1];
}

int Puzzle::getWhiteOnRow(int row) {
    return _rowInformations[row][2];
}

int Puzzle::getBlackOnColumn(int column) {
    return _columnInformations[column][1];
}

int Puzzle::getWhiteOnColumn(int column) {
    return _columnInformations[column][2];
}

int Puzzle::getNum() {
    return _num;
}

bool Puzzle::getValidPuzzle() {
    return _validPuzzle;
}

Puzzle::Difficulty Puzzle::getDifficulty() {
    return _difficulty;
}

bool Puzzle::greenRow(int row) {
    return _rowInformations[row][0] == 1;
}

bool Puzzle::greenColumn(int column) {
    return _columnInformations[column][0] == 1;
}

void Puzzle::printPuzzle() {
    std::cout << "----- PUZZLE -----"  << std::endl;
    std::cout << "Size : " << _size << std::endl;
    std::cout << "Difficulty : " << (int)_difficulty << std::endl;
    std::cout << "N° : " << _num << std::endl;
    std::cout << "------------------------------ " << std::endl;
    std::cout << " \n ___Current Puzzle___ " << std::endl;
    for (int i=0; i<_size; i++) {
        for (int j=0; j<_size; j++) {
            std::cout << " | " << _currentPuzzle[i][j];
        }
        std::cout << " | " << std::endl;
    }

    std::cout << "\n Validation PuzzleRow      Validation PuzzleColumn     " <<
                 "Validation Puzzle" << std::endl;

    for (int i=0; i<_size; i++) {
        for (int j=0; j<_size; j++) {
            std::cout << " | " << _validationPuzzleRow[i][j];
        }
        std::cout << " |    " ; // << std::endl;

        for (int j=0; j<_size; j++) {
            std::cout << " | " << _validationPuzzleColumn[i][j];
        }
        std::cout << " |    "; // << std::endl;

        for (int j=0; j<_size; j++) {
            std::cout << " | " << _validationPuzzle[i][j];
        }
        std::cout << " |    " << std::endl;
    }

    std::cout<< "\ninfoRow     infoColumn"<<std::endl;
    for (int j=0; j<_size; j++) {
        std::cout << "   " << j;
    }
    std::cout << "      " ; // << std::endl;

    for (int j=0; j<_size; j++) {
        std::cout << "   " << j;
    }
    std::cout << std::endl;
    for (int j=0; j<_size*2; j++) {
        std::cout << "-----";
    }
    std::cout << std::endl;

    for (int i=0; i<3; i++) {
        for (int j=0; j<_size; j++) {
            std::cout << " | " << _rowInformations[j][i];
        }
        std::cout << " |    " ; // << std::endl;

        for (int j=0; j<_size; j++) {
            std::cout << " | " << _columnInformations[j][i];
        }
        std::cout << " |    " << std::endl;
    }
    std::cout << "\n-- END PRINT --"  << std::endl;
    std::cout << "Valid = " << _validPuzzle << std::endl;
}




