#include "takuzumodel.h"

TakuzuModel::TakuzuModel() {
    _numberOfUndo = 0;
    _totalGameTime = 0;
}

void TakuzuModel::loadPuzzle(Puzzle::Difficulty difficulty, int size, int num) {
    _puzzle = PuzzleManager::getPuzzle(difficulty, size, num);
}

void TakuzuModel::updateCell(int row, int column, CaseWidget::Value oldValue, CaseWidget::Value newValue) {
    CommandClick* command = new CommandClick(row, column, oldValue, newValue);
    commandsUndo->push(command);
    connect(command, SIGNAL(setValue(int, int, CaseWidget::Value)),
            this, SIGNAL(setValue(int, int, CaseWidget::Value)));
    connect(command, SIGNAL(updateCell(int, int, CaseWidget::Value)),
            this, SLOT(updateCell(int, int, CaseWidget::Value)));
    command->execute();

    updateCell(row, column, newValue);
}

void TakuzuModel::updateCell(int row, int col, CaseWidget::Value value) {
    _puzzle->updateCell(row, col, (Puzzle::Value)value);
    sendValidationPuzzle();
    sendColorCount(row, col);
    if (_puzzle->getValidPuzzle()) {
        emit endGame();
        emit setEndMsg(_numberOfUndo, getTotalGameTime());
    }
}

void TakuzuModel::undo() {
    if (!commandsUndo->empty()) {
        commandsUndo->top()->undo();
        commandsRedo->push(commandsUndo->top());
        commandsUndo->pop();
        _numberOfUndo++;
    }
}

void TakuzuModel::redo() {
    if (!commandsRedo->empty()) {
        commandsRedo->top()->redo();
        commandsUndo->push(commandsRedo->top());
        commandsRedo->pop();
    }
}

void TakuzuModel::sendColorCount(int row, int column) {
   emit setColorCountRow(row, _puzzle->getBlackOnRow(row), _puzzle->getWhiteOnRow(row));
   emit setColorCountColumn(column, _puzzle->getBlackOnColumn(column),
                             _puzzle->getWhiteOnColumn(column));
}

void TakuzuModel::sendValidationPuzzle() {
    int ** validationPuzzle = _puzzle->getValidationPuzzle();
    for (int i=0; i<_puzzle->getSize(); i++) {
        emit correctRow(_puzzle->greenRow(i), i);
        emit correctColumn(_puzzle->greenColumn(i), i);
        for (int j=0; j<_puzzle->getSize(); j++) {
            emit setValidation(i, j, validationPuzzle[i][j]);
        }
    }
}

void TakuzuModel::sendInitialPuzzle() {
    int** currentPuzzle = _puzzle->getCurrentPuzzle();
    int widgetSize;
    switch (_puzzle->getSize()) {
    case 6:
        widgetSize = 60;
        break;
    case 8:
        widgetSize = 44;
        break;
    case 10:
        widgetSize = 34;
        break;
    }
    for (int i = 0; i < _puzzle->getSize(); i++) {
        for (int j = 0; j < _puzzle->getSize(); j++) {
            if (currentPuzzle[i][j] == 1) {
                emit addCase(i, j, widgetSize, CaseWidget::BLACK);
            } else if (currentPuzzle[i][j] == 2) {
                emit addCase(i, j, widgetSize, CaseWidget::WHITE);
            } else {
                emit addCase(i, j, widgetSize, CaseWidget::EMPTY);
            }
        }
    }
    emit setDifficultyLabel(_puzzle->getDifficulty());
    emit setSizeLabel(_puzzle->getSize());
    emit setPuzzleNumberLabel(_puzzle->getNum(),
                              PuzzleManager::getTotalPuzzles(_puzzle->getDifficulty(),
                                                             _puzzle->getSize()));
    emit startTimer();
}

void TakuzuModel::sendInitialColorCount() {
    for (int i=0; i<_puzzle->getSize(); i++) {
        sendColorCount(i,i);
    }
}

void TakuzuModel::reset() {
    while (!commandsUndo->empty()) {
        undo();
    }
    emit startTimer();
    _numberOfUndo = 0;
    delete commandsRedo;
    commandsRedo = new std::stack<Command*>;
}

void TakuzuModel::addToTotalGameTime(qint64 gameTime) {
    _totalGameTime += gameTime;
}

void TakuzuModel::randomLevel() {
    srand(time(nullptr));

    int size = 2*(rand()%(3)+3);
    int difficulty = rand()%2;
    int nbLevel = PuzzleManager::getTotalPuzzles((Puzzle::Difficulty)difficulty, size);
    int level = rand()%nbLevel+1;

    emit goToGame(size, (Puzzle::Difficulty)difficulty, level);
}

int TakuzuModel::getNumberOfUndo() {
    return _numberOfUndo;
}

qint64 TakuzuModel::getTotalGameTime() {
    return _totalGameTime;
}

void TakuzuModel::replay() {
    reset();
}

void TakuzuModel::nextPuzzle() {
    int currentLevel = _puzzle->getNum();
    int currentSize = _puzzle->getSize();
    Puzzle::Difficulty currentDifficulty = _puzzle->getDifficulty();
    if (currentLevel < PuzzleManager::getTotalPuzzles(_puzzle->getDifficulty(), _puzzle->getSize())) {
        emit goToGame(currentSize, currentDifficulty, currentLevel+1);
        return;
    } else if (currentDifficulty == Puzzle::EASY) {
        emit goToGame(currentSize, Puzzle::HARD, 1);
        return;
    } else if (currentSize < 10) {
        emit goToGame(currentSize+2, Puzzle::EASY, 1);
        return;
    } else {
        emit goToMenu();
    }
}
