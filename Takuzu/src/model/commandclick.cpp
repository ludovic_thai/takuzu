#include "commandclick.h"

CommandClick::CommandClick(int row, int col, CaseWidget::Value oldValue, CaseWidget::Value newValue) {
    _row = row;
    _col = col;
    _oldValue = oldValue;
    _newValue = newValue;
}

void CommandClick::execute() {
    emit setValue(_row, _col, _newValue);
}

void CommandClick::undo() {
    emit setValue(_row, _col, _oldValue);
    updateCell(_row, _col, _oldValue);
}

void CommandClick::redo() {
    emit setValue(_row, _col, _newValue);
    updateCell(_row, _col, _newValue);
}
