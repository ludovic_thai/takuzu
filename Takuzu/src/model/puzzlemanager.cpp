#include "puzzlemanager.h"

int PuzzleManager::getTotalPuzzles(Puzzle::Difficulty difficulty, int size) {
    std::string allias = std::to_string(size) + '_';

    switch (difficulty) {
    case Puzzle::EASY:
        allias += "easy";
        break;
    case Puzzle::HARD:
        allias += "hard";
        break;
    }

    QString path = QString::fromStdString(":/puzzles/" + allias);

    QFileSelector selector;
    QFile puzzlesFile(selector.select(path));

    if (puzzlesFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return puzzlesFile.readLine().toInt();
    } else {
        return -1;
    }
}

Puzzle* PuzzleManager::getPuzzle(Puzzle::Difficulty difficulty, int size, int num) {
    char** puzzle = new char*[size];
    for (int i = 0; i < size; i++) {
        puzzle[i] = new char[size];
    }

    std::string allias = std::to_string(size) + '_';

    switch (difficulty) {
    case Puzzle::EASY:
        allias += "easy";
        break;
    case Puzzle::HARD:
        allias += "hard";
        break;
    }

    QString path = QString::fromStdString(":/puzzles/" + allias);

    QFileSelector selector;
    QFile puzzlesFile(selector.select(path));

    if (puzzlesFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        int i = 0;
        do {
            puzzlesFile.readLine();
            i++;
        } while (i < num);
        return new Puzzle(difficulty, size, num, puzzlesFile.readLine().toStdString());
    } else {
        return nullptr;
    }
}
