#include "presenter.h"

Presenter::Presenter() {}

void Presenter::setGameModel(TakuzuModel *gameModel) {
    _gameModel = gameModel;
}

void Presenter::setViewMenu(ViewMenu *viewMenu) {
    _viewMenu = viewMenu;
}

void Presenter::setViewGame(ViewGame *viewGame) {
    _viewGame = viewGame;
}

void Presenter::connectAll() {
    connectModeleToPresenter();
    connectViewToPresenter();
    connectModelToView();
    connectViewToModel();
}

void Presenter::connectModeleToPresenter() {
    connect(_gameModel, SIGNAL(goToGame(int, Puzzle::Difficulty, int)),
            this, SLOT(goToGame(int, Puzzle::Difficulty, int)));
    connect(_gameModel, SIGNAL(goToMenu()),
            this, SLOT(goToMenu()));
}

void Presenter::connectViewToPresenter() {
    connect(_viewMenu, SIGNAL(goToGame(int, Puzzle::Difficulty, int)),
            this, SLOT(goToGame(int, Puzzle::Difficulty, int)));
    connect(_viewGame, SIGNAL(goToMenu()),
            this, SLOT(goToMenu()));
    connect(_viewMenu, SIGNAL(openRules()),
           this, SLOT(openRules()));
    connect(_viewGame, SIGNAL(openRules()),
            this, SLOT(openRules()));
}


void Presenter::connectModelToView() {
    connect(_gameModel, SIGNAL(addCase(int, int, int, CaseWidget::Value)),
            _viewGame, SLOT(addCase(int, int, int, CaseWidget::Value)));
    connect(_gameModel, SIGNAL(setValue(int, int, CaseWidget::Value)),
            _viewGame, SLOT(setValue(int, int, CaseWidget::Value)));
    connect(_gameModel, SIGNAL(setValidation(int, int, int)),
            _viewGame, SLOT(setValidation(int, int, int)));
    connect(_gameModel, SIGNAL(setDifficultyLabel(Puzzle::Difficulty)),
            _viewGame, SLOT(setDifficultyLabel(Puzzle::Difficulty)));
    connect(_gameModel, SIGNAL(setSizeLabel(int)),
            _viewGame, SLOT(setSizeLabel(int)));
    connect(_gameModel, SIGNAL(setPuzzleNumberLabel(int, int)),
            _viewGame, SLOT(setPuzzleNumberLabel(int, int)));
    connect(_gameModel, SIGNAL(setColorCountRow(int, int, int)),
            _viewGame, SLOT(setColorCountRow(int, int ,int)));
    connect(_gameModel, SIGNAL(setColorCountColumn(int, int, int)),
            _viewGame, SLOT(setColorCountColumn(int, int ,int)));
    connect(_gameModel, SIGNAL(correctRow(bool, int)),
            _viewGame, SLOT(toggleColorCountRow(bool, int)));
    connect(_gameModel, SIGNAL(correctColumn(bool, int)),
            _viewGame, SLOT(toggleColorCountCol(bool, int)));
    connect(_gameModel, SIGNAL(startTimer()),
            _viewGame, SLOT(startTimer()));
    connect(_gameModel, SIGNAL(endGame()),
            _viewGame, SLOT(endGame()));
    connect(_gameModel, SIGNAL(setEndMsg(int, qint64)),
            _viewGame, SLOT(setEndMsg(int, qint64)));
}

void Presenter::connectViewToModel() {
    connect(_viewGame, SIGNAL(updateCell(int, int, CaseWidget::Value, CaseWidget::Value)),
            _gameModel, SLOT(updateCell(int, int, CaseWidget::Value, CaseWidget::Value)));
    connect(_viewGame, SIGNAL(undo()),
            _gameModel, SLOT(undo()));
    connect(_viewGame, SIGNAL(redo()),
            _gameModel, SLOT(redo()));
    connect(_viewGame, SIGNAL(resetPuzzle()),
            _gameModel, SLOT(reset()));
    connect(_viewGame, SIGNAL(elapsedTime(qint64)),
            _gameModel, SLOT(addToTotalGameTime(qint64)));
    connect(_viewMenu, SIGNAL(randomLevel()),
            _gameModel, SLOT(randomLevel()));
    connect(_viewGame, SIGNAL(replay()),
            _gameModel, SLOT(replay()));
    connect(_viewGame, SIGNAL(nextPuzzle()),
            _gameModel, SLOT(nextPuzzle()));
}

void Presenter::goToGame(int size, Puzzle::Difficulty difficulty, int level) {
    _viewMenu->hide();
    _gameModel->loadPuzzle(difficulty, size, level);
    _gameModel->sendInitialPuzzle();
    _viewGame->setCountWidgets();
    _gameModel->sendInitialColorCount();
    _viewGame->show();
}

void Presenter::goToMenu() {
    _viewGame->hide();
    _viewMenu->goToMainMenu();
    _viewMenu->show();
}

void Presenter::openRules() {
    ViewGameRules * viewRules = new ViewGameRules();
    viewRules->show();
}
