#include "viewGame.h"
#include <QApplication>
#include "puzzle.h"
#include <iostream>
#include "presenter.h"
#include "takuzumodel.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    TakuzuModel* model = new TakuzuModel;
    Presenter* presenter = new Presenter;
    ViewMenu* viewMenu = new ViewMenu;
    ViewGame* viewGame = new ViewGame;

    presenter->setGameModel(model);
    presenter->setViewMenu(viewMenu);
    presenter->setViewGame(viewGame);
    presenter->connectAll();

    viewMenu->show();

    return app.exec();
}
