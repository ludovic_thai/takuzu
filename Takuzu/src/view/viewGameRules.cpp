#include "viewGameRules.h"
#include "ui_viewGameRules.h"

ViewGameRules::ViewGameRules(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewGameRules) {
    ui->setupUi(this);
    setFixedSize(613,655);
    setWindowTitle("Takuzy Game Rules");

}

ViewGameRules::~ViewGameRules() {
    delete ui;
}

void ViewGameRules::on_pushButton_clicked() {
    close();
}

void ViewGameRules::on_actionClose_triggered() {
    close();
}
