#include "levelwidget.h"
#include <QPushButton>
#include <string>

levelWidget::levelWidget(QWidget *parent) : QWidget(parent) {
    layout = new QGridLayout();
    layout->setContentsMargins(75, 50, 75, 50);
    setLayout(layout);

    buttonGroup = new QButtonGroup();

    QPushButton* buttonRetour = new QPushButton("Back", this);
    buttonRetour->setFixedSize(61, 31);
    buttonRetour->move(20, 400);

    connect(buttonGroup, SIGNAL(buttonClicked(int)), this, SIGNAL(levelChoose(int)));
    connect(buttonRetour, SIGNAL(clicked()), this, SIGNAL(returnToPreviousMenu()));
}

void levelWidget::setNbLevel(int nb) {
    for (int i = 0; i < nb; i++) {
        QPushButton* button = new QPushButton(QString::number(i+1));
        layout->addWidget(button, 1 + i/6, i%6);
        buttonGroup->addButton(button, i+1);
    }
}
