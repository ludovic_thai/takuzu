#include "viewGame.h"
#include "ui_viewGame.h"
#include "countwidget.h"
#include <QTimer>
#include <QMessageBox>

ViewGame::ViewGame(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::ViewGame),
      tick(new QTimer) {
    ui->setupUi(this);
    ui->endWidget->hide();
    ui->pauseLabel->hide();
    ui->timer->display("00:00:00");

    connect(tick, &QTimer::timeout, this, &ViewGame::showTime);

    setFixedSize(610, 760);
    setWindowTitle("Takuzu Game");

    // To choose the symbole in the menu
    QActionGroup*  displayGroup = new QActionGroup(this);
    QAction* circle = new QAction("&Circle");
    circle->setCheckable(true);
    circle->setChecked(true);
    QAction* square = new QAction("&Square");
    square->setCheckable(true);
    QAction* binary = new QAction("&Binary");
    binary->setCheckable(true);
    QAction* tictactoe = new QAction("&TicTacToe");
    tictactoe->setCheckable(true);

    displayGroup->addAction(circle);
    displayGroup->addAction(square);
    displayGroup->addAction(binary);
    displayGroup->addAction(tictactoe);
    displayGroup->setExclusive(true);

    QMenu* menuStyle = new QMenu("&Style", ui->menubar);
    menuStyle->addAction(circle);
    menuStyle->addAction(square);
    menuStyle->addAction(binary);
    menuStyle->addAction(tictactoe);

    ui->menubar->insertMenu(ui->menubar->actions()[1], menuStyle);

    connect(circle, SIGNAL(triggered()), this, SLOT(displayCircle()));
    connect(square, SIGNAL(triggered()), this, SLOT(displaySquare()));
    connect(binary, SIGNAL(triggered()), this, SLOT(displayBinary()));
    connect(tictactoe, SIGNAL(triggered()), this, SLOT(displayTicTacToe()));
}

ViewGame::~ViewGame() {
    delete ui;
}

void ViewGame::addCase(int x, int y, int size, CaseWidget::Value defaultValue) {
    CaseWidget* c = new CaseWidget(x, y, size, defaultValue, _display);
    ui->gridLayout->addWidget(c, x, y);

    connect(c, SIGNAL(updateCell(int, int, CaseWidget::Value, CaseWidget::Value)),
            this, SIGNAL(updateCell(int, int, CaseWidget::Value, CaseWidget::Value)));
}

void ViewGame::setValue(int row, int col, CaseWidget::Value value) {
    CaseWidget* c = (CaseWidget*)ui->gridLayout->itemAtPosition(row, col)->widget();
    c->setValue(value);
}

void ViewGame::setCountWidgets() {
    for (int i = 0; i < _size; i++) {
        CountWidget* countWidget = new CountWidget;
        countWidget->setFixedSize(800/_size, 300/_size);
        ui->gridLayoutRow->addWidget(countWidget, i, 0);
    }

    for (int i = 0; i < _size; i++) {
        CountWidget* countWidget = new CountWidget;
        countWidget->setFixedSize(300/_size, 800/_size - 5);
        ui->gridLayoutCol->addWidget(countWidget, 0, i);
    }
}

void ViewGame::setColorCountRow(int x, int blackCount, int whiteCout) {
    CountWidget* c = (CountWidget*)ui->gridLayoutRow->itemAtPosition(x, 0)->widget();
    c->setNb(whiteCout, blackCount);
}

void ViewGame::setColorCountColumn(int x, int blackCount, int whiteCout) {
    CountWidget* c = (CountWidget*)ui->gridLayoutCol->itemAtPosition(0, x)->widget();
    c->setNb(whiteCout, blackCount);
}

void ViewGame::toggleColorCountRow(bool hide, int row) {
    CountWidget* c = (CountWidget*)ui->gridLayoutRow->itemAtPosition(row, 0)->widget();
    c->hide(hide);
}

void ViewGame::toggleColorCountCol(bool hide, int col) {
    CountWidget* c = (CountWidget*)ui->gridLayoutCol->itemAtPosition(0, col)->widget();
    c->hide(hide);
}

void ViewGame::setValidation(int x, int y, int validation) {
    CaseWidget* c = (CaseWidget*)ui->gridLayout->itemAtPosition(x, y)->widget();
    c->setValidation((CaseWidget::validationValue) validation);
    c->update();
}

void ViewGame::setSizeLabel(int size) {
    _size = size;
    ui->sizeLabel->setText("Size : " + QString::number(size) + " x " + QString::number(size));
}

void ViewGame::setPuzzleNumberLabel(int puzzleNumber, int total) {
    ui->puzzelNuberLabel->setText("Puzzle : " + QString::number(puzzleNumber) + " / " + QString::number(total));
}

void ViewGame::setDifficultyLabel(Puzzle::Difficulty difficulty) {
    switch(difficulty) {
    case Puzzle::EASY :
        ui->difficultyLabel->setText("Difficulty : EASY");
        break;
    case Puzzle::HARD :
        ui->difficultyLabel->setText("Difficulty : HARD");
        break;
    default:
        break;
    }
}

void ViewGame::on_undoButton_clicked() {
    emit undo();
}

void ViewGame::on_redoButton_clicked() {
    emit redo();
}

void ViewGame::on_resetGameButton_clicked() {
    ui->pauseLabel->hide();
    ui->pause->setText("Pause");
    _pauseTime = 0;
    emit resetPuzzle();
}

void ViewGame::showTime() {
    qint64 time = _timer.elapsed();
    ui->timer->display(timeQint64ToQString(time + _pauseTime));
}

void ViewGame::startTimer() {
    _timer.start();
    tick->start();
}

void ViewGame::endGame() {
    tick->stop();
    emit elapsedTime(_pauseTime +_timer.elapsed());
    _pauseTime = 0;
    ui->endWidget->show();
}

QString ViewGame::timeQint64ToQString(qint64 time) {
    qint64 seconds = (time/1000)%60;
    qint64 minutes = (time/(1000*60))%60;
    qint64 hours = (time/(1000*3600))%60;

    QString ss;
    QString mm;
    QString hh;

    if (seconds/10) {
        ss = QString::number(seconds);
    } else {
        ss = "0" + QString::number(seconds);
    }
    if (minutes/10) {
        mm = QString::number(minutes);
    } else {
        mm = "0" + QString::number(minutes);
    }
    if (hours/10) {
        hh = QString::number(hours);
    } else {
        hh = "0" + QString::number(hours);
    }

    return hh+":"+mm+":"+ss;
}

void ViewGame::reset() {
    for (int i = 0; i < _size; i++) {
        for (int j = 0; j < _size; j++) {
            delete ui->gridLayout->itemAtPosition(i, j)->widget();
        }
    }

    for (int i = 0; i < _size; i++) {
        delete ui->gridLayoutCol->itemAtPosition(0, i)->widget();
    }

    for (int i = 0; i < _size; i++) {
        delete ui->gridLayoutRow->itemAtPosition(i, 0)->widget();
    }

    _pauseTime = 0;
    ui->pauseLabel->hide();
    ui->pause->setText("Pause");
}

void ViewGame::returnToMenu() {
    ui->endWidget->hide();
    reset();
    emit goToMenu();
}

void ViewGame::replayPuzzle() {
    ui->endWidget->hide();
    emit replay();
}

void ViewGame::goToNextPuzzle() {
    ui->endWidget->hide();
    reset();
    emit nextPuzzle();
}

void ViewGame::setEndMsg(int nbUndo, qint64 time) {
    QString qtime = timeQint64ToQString(time);
    QString msg = "You finished the level in " + qtime + " with a total of " +
            QString::number(nbUndo) + " undo.";
    ui->endWidget->setEndMsg(msg);
}

void ViewGame::on_pause_clicked()
{
    if (ui->pause->text() == "Pause") {
        ui->pauseLabel->show();
        tick->stop();
        _pauseTime += _timer.elapsed();
        ui->pause->setText("Play");
    } else {
        ui->pauseLabel->hide();
        startTimer();
        ui->pause->setText("Pause");
    }
}

void ViewGame::quit() {
   close();
}

void ViewGame::on_actionReset_Game_triggered() {
    ui->pauseLabel->hide();
    ui->pause->setText("Pause");
    _pauseTime = 0;
    emit resetPuzzle();
}

void ViewGame::on_actionExit_triggered() {
   close();
}

void ViewGame::on_actionUndo_triggered() {
    emit undo();
}

void ViewGame::on_actionRedo_triggered() {
    emit redo();
}

void ViewGame::on_actionRules_triggered() {
    emit openRules();
}

void ViewGame::on_actionAbout_triggered() {
    QMessageBox aboutMessage;
    aboutMessage.setWindowTitle("About the Takuzu Game");
    aboutMessage.setText("Takuzu (c) 2020\nAuthors: Ludovic Thai & Benjamin Vignaux");
    aboutMessage.exec();
}

void ViewGame::setDisplay(CaseWidget::Display display) {
    _display = display;

    for(int i = 0; i < _size; i++) {
        for (int j = 0; j < _size; j++) {
            CaseWidget* c = (CaseWidget*)ui->gridLayout->itemAtPosition(i, j)->widget();
            c->setDisplay(_display);
        }
    }
}

void ViewGame::displayCircle() {
    setDisplay(CaseWidget::CIRCLE);
}

void ViewGame::displaySquare() {
    setDisplay(CaseWidget::SQUARE);
}

void ViewGame::displayBinary() {
    setDisplay(CaseWidget::BINARY);
}

void ViewGame::displayTicTacToe() {
    setDisplay(CaseWidget::TICTACTOE);
}
