#include "countwidget.h"
#include <QPainter>

CountWidget::CountWidget(QWidget *parent) : QWidget(parent) {}

void CountWidget::setNb(int nbWhite, int nbBlack) {
    _nbWhite = nbWhite;
    _nbBlack = nbBlack;
    update();
}

void CountWidget::hide(bool hide) {
    _hide = hide;
    update();
}

void CountWidget::paintEvent(QPaintEvent *event) {
    if (_hide) {
        return;
    }

    QPainter* painter = new QPainter(this);

    QRect* rect1;
    QRect* rect2;

    int circleWidth;

    if (width() > height()) {
        painter->drawLine(width()/2, 0, width()/2, height()-1);
        circleWidth = std::min(width()/2, height()) - 1;
        rect1 = new QRect((width()/2 - circleWidth)/2, (height() - circleWidth)/2, circleWidth, circleWidth);
        rect2 = new QRect(rect1->x() + width()/2, rect1->y(), circleWidth, circleWidth);
    } else {
        painter->drawLine(0, height()/2, width()-1, height()/2);
        circleWidth = std::min(width(), height()/2) - 1;
        rect1 = new QRect((width() - circleWidth)/2, (height()/2 - circleWidth)/2, circleWidth, circleWidth);
        rect2 = new QRect(rect1->x(), rect1->y() + height()/2, circleWidth, circleWidth);
    }
    painter->setBrush(QBrush(Qt::white));
    painter->drawEllipse(*rect1);
    painter->setBrush(QBrush(Qt::black));
    painter->drawEllipse(*rect2);

    painter->setPen(Qt::black);
    painter->drawText(*rect1, Qt::AlignCenter, QString::number(_nbWhite));
    painter->setPen(Qt::white);
    painter->drawText(*rect2, Qt::AlignCenter, QString::number(_nbBlack));

    painter->end();
}
