#include "viewMenu.h"
#include "ui_viewMenu.h"
#include "puzzlemanager.h"
#include <QMessageBox>

ViewMenu::ViewMenu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewMenu) {
    ui->setupUi(this);
    setFixedSize(575, 530);
    setWindowTitle("Takuzu Game - Menu");

    for (auto button : ui->buttonGroup->buttons()) {
        if (button == ui->button6) {
            ui->buttonGroup->setId(button, 6);
        } else if (button == ui->button8) {
            ui->buttonGroup->setId(button, 8);
        } else {
            ui->buttonGroup->setId(button, 10);
        }
    }

    for (auto button : ui->buttonGroup_2->buttons()) {
        if (button == ui->buttonEasy) {
            ui->buttonGroup_2->setId(button, 0);
        } else {
            ui->buttonGroup_2->setId(button, 1);
        }
    }

    connect(ui->stackedWidget->widget(3), SIGNAL(levelChoose(int)), this, SLOT(goToGame(int)));
    connect(ui->stackedWidget->widget(3), SIGNAL(returnToPreviousMenu()), this, SLOT(returnToPreviousMenu()));
}

ViewMenu::~ViewMenu() {
    delete ui;
}

void ViewMenu::returnToPreviousMenu() {
    int index = ui->stackedWidget->currentIndex() - 1;
    ui->stackedWidget->setCurrentIndex(index);
}

void ViewMenu::on_buttonPlay_clicked() {
    ui->stackedWidget->setCurrentIndex(1);
}

void ViewMenu::goToMainMenu() {
    ui->stackedWidget->setCurrentIndex(0);
}

void ViewMenu::goToDifficultyMenu(int size) {
    _size = size;
    ui->stackedWidget->setCurrentIndex(2);
}

void ViewMenu::goToLevelMenu(int difficulty) {
    _difficulty = (Puzzle::Difficulty)difficulty;
    _nbLevel = PuzzleManager::getTotalPuzzles(_difficulty, _size);
    ui->stackedWidget->setCurrentIndex(3);

    levelWidget* widget = (levelWidget*)ui->stackedWidget->currentWidget();
    widget->setNbLevel(_nbLevel);
    widget->update();
}

void ViewMenu::goToGame(int level) {
    _level = level;
    emit goToGame(_size, _difficulty, _level);
}

void ViewMenu::on_buttonRandom_clicked() {
    emit randomLevel();
}

void ViewMenu::on_buttonRules_clicked() {
    emit openRules();
}

void ViewMenu::on_buttonAbout_clicked() {
    QMessageBox aboutMessage;
    aboutMessage.setWindowTitle("About the Takuzu Game");
    aboutMessage.setText("Takuzu (c) 2020\nAuthors: Ludovic Thai & Benjamin Vignaux");
    aboutMessage.exec();
}

void ViewMenu::on_buttonQuit_clicked() {
    close();
}
