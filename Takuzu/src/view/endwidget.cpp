#include "endwidget.h"
#include "ui_endwidget.h"

EndWidget::EndWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EndWidget) {
    ui->setupUi(this);
}

EndWidget::~EndWidget() {
    delete ui;
}

void EndWidget::setEndMsg(QString msg) {
    ui->endMsg->setText(msg);
}

void EndWidget::on_returnToMenu_clicked() {
    emit returnToMenu();
}

void EndWidget::on_replay_clicked() {
    emit replay();
}

void EndWidget::on_nextPuzzle_clicked() {
    emit nextPuzzle();
}

void EndWidget::on_pushButton_clicked() {
    emit quit();
}
