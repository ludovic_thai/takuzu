#include "casewidget.h"

CaseWidget::CaseWidget(int x, int y, int size, Value defaultValue, Display display, QWidget *parent) :
    QWidget(parent),
    _value(EMPTY), _display(display) {
    _x = x;
    _y = y;

    setFixedWidth(size);
    setFixedHeight(size);

    switch (defaultValue) {
    case EMPTY:
        _editable = true;
        break;
    default:
        _value = defaultValue;
        _editable = false;
        repaint();
    }
}

void CaseWidget::mousePressEvent(QMouseEvent *event) {
    if (!_editable) {
        return;
    }

    Value value;
    if (event->button() == Qt::LeftButton) {
        value = (Value)((_value+1)%3);
        emit updateCell(_x, _y, _value, value);
    } else if (event->button() == Qt::RightButton) {
        value = (Value)((_value+2)%3);
        emit updateCell(_x, _y, _value, value);
    }
}

void CaseWidget::setValue(Value value) {
    _value = value;
    repaint();
}

void CaseWidget::setValidation(validationValue validation) {
    _validation = validation;
    repaint();
}

void CaseWidget::setDisplay(Display display) {
    _display = display;
    repaint();
}

void CaseWidget::paintEvent(QPaintEvent *event) {
    QPainter* painter = new QPainter(this);
    QFont* font = new QFont();
    font->setPointSize(0.45*width());

    if (_validation == CORRECT) {
        painter->fillRect(0, 0, width()-1, height()-1, QBrush(Qt::green, Qt::SolidPattern));
    } else if (_validation == INCORRECT) {
        painter->fillRect(0, 0, width()-1, height()-1, QBrush(Qt::red, Qt::SolidPattern));
    } else {
        painter->fillRect(0, 0, width()-1, height()-1, QBrush(Qt::gray, Qt::SolidPattern));
    }

    painter->drawRect(0, 0, width()-1, height()-1);

    switch (_display) {
    case CIRCLE:
        if (_value == BLACK) {
            painter->setBrush(QBrush(Qt::black));
            painter->drawEllipse(width()/8, height()/8, 0.75*width(), 0.75*height());
            if (!_editable) {
                painter->setBrush(QBrush(Qt::white));
                painter->drawEllipse(width()*0.45, height()*0.45, 0.1*width(), 0.1*height());
            }
        } else if (_value == WHITE) {
            painter->setBrush(QBrush(Qt::white));
            painter->drawEllipse(width()/8, height()/8, 0.75*width(), 0.75*height());
            if (!_editable) {
                painter->setBrush(QBrush(Qt::black));
                painter->drawEllipse(width()*0.45, height()*0.45, 0.1*width(), 0.1*height());
            }
        }
        break;

    case SQUARE:
        if (_value == BLACK) {
            painter->setBrush(QBrush(Qt::black));
            painter->drawRect(width()/8, height()/8, 0.75*width(), 0.75*height());
            if (!_editable) {
                painter->setBrush(QBrush(Qt::white));
                painter->drawRect(width()*0.45, height()*0.45, 0.1*width(), 0.1*height());
            }
        } else if (_value == WHITE) {
            painter->setBrush(QBrush(Qt::white));
            painter->drawRect(width()/8, height()/8, 0.75*width(), 0.75*height());
            if (!_editable) {
                painter->setBrush(QBrush(Qt::black));
                painter->drawRect(width()*0.45, height()*0.45, 0.1*width(), 0.1*height());
            }
        }
        break;

    case BINARY:
        painter->setPen(Qt::black);
        if (!_editable) {
            font->setUnderline(true);
        }
        painter->setFont(*font);

        if (_value == BLACK) {
            painter->drawText(width()/8, height()/8, 0.75*width(), 0.75*height(), Qt::AlignCenter, "1");
        } else if (_value == WHITE) {
            painter->drawText(width()/8, height()/8, 0.75*width(), 0.75*height(), Qt::AlignCenter, "0");
        }
        break;

    case TICTACTOE:
        painter->setPen(Qt::black);
        if (!_editable) {
            font->setUnderline(true);
        }
        painter->setFont(*font);

        if (_value == BLACK) {
            painter->drawText(width()/8, height()/8, 0.75*width(), 0.75*height(), Qt::AlignCenter, "X");
        } else if (_value == WHITE) {
            painter->drawText(width()/8, height()/8, 0.75*width(), 0.75*height(), Qt::AlignCenter, "O");
        }
        break;
    }
    painter->end();
}
