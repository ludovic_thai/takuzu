QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
    include/model \
    include/view \
    include/presenter

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/view/casewidget.cpp \
    src/model/command.cpp \
    src/model/commandclick.cpp \
    src/view/countwidget.cpp \
    src/view/endwidget.cpp \
    src/view/levelwidget.cpp \
    src/main.cpp \
    src/presenter/presenter.cpp \
    src/model/puzzle.cpp \
    src/model/puzzlemanager.cpp \
    src/model/takuzumodel.cpp \
    src/view/viewGame.cpp \
    src/view/viewGameRules.cpp \
    src/view/viewMenu.cpp

HEADERS += \
    include/view/casewidget.h \
    include/model/command.h \
    include/model/commandclick.h \
    include/view/countwidget.h \
    include/view/endwidget.h \
    include/view/levelwidget.h \
    include/presenter/presenter.h \
    include/model/puzzle.h \
    include/model/puzzlemanager.h \
    include/model/takuzumodel.h \
    include/view/viewGame.h \
    include/view/viewGameRules.h \
    include/view/viewMenu.h

FORMS += \
    forms/endwidget.ui \
    forms/viewGame.ui \
    forms/viewGameRules.ui \
    forms/viewMenu.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res/puzzles.qrc
