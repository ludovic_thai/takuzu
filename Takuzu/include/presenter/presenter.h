#ifndef PRESENTER_H
#define PRESENTER_H

#include "takuzumodel.h"
#include "viewGame.h"
#include "viewMenu.h"
#include "ui_viewGame.h"
#include "casewidget.h"
#include "QObject"
#include "viewGameRules.h"

class Presenter: QObject
{
    Q_OBJECT
public:
    Presenter();
    void setGameModel(TakuzuModel* gameModel);
    void setViewMenu(ViewMenu* viewMenu);
    void setViewGame(ViewGame* viewGame);

    void connectAll();
    void connectModelToView();
    void connectViewToModel();
    void connectViewToPresenter();
    void connectModeleToPresenter();

public slots:
    void goToGame(int size, Puzzle::Difficulty difficulty, int level);
    void goToMenu();
    void openRules();

private:
    TakuzuModel* _gameModel;
    ViewMenu* _viewMenu;
    ViewGame* _viewGame;
};

#endif // PRESENTER_H
