#ifndef CASEWIDGET_H
#define CASEWIDGET_H

#include <QWidget>
#include <QMouseEvent>
#include <QPainter>

class CaseWidget : public QWidget
{
    Q_OBJECT
public:
    enum Value {
        EMPTY,
        BLACK,
        WHITE
    };

    enum validationValue {
        NONE,
        CORRECT,
        INCORRECT
    };

    enum Display {
        CIRCLE,
        SQUARE,
        BINARY,
        TICTACTOE
    };

    explicit CaseWidget(int x, int y, int size, Value defaultValue, Display display, QWidget *parent = nullptr);

    void setValue(Value value);

    void setValidation(validationValue validation);

    void setDisplay(Display display);

    void mousePressEvent(QMouseEvent *event) override;

private:
    Value _value;
    validationValue _validation = NONE;
    Display _display;
    int _x;
    int _y;
    bool _editable;

protected:
    void paintEvent(QPaintEvent *event) override;

signals:
    void updateCell(int x, int y, CaseWidget::Value oldValue, CaseWidget::Value newValue);
};

#endif // CASEWIDGET_H
