#ifndef VIEWMENU_H
#define VIEWMENU_H

#include <QMainWindow>
#include "puzzle.h"

namespace Ui {
class ViewMenu;
}

class ViewMenu : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewMenu(QWidget *parent = nullptr);
    ~ViewMenu();

signals:
    void goToGame(int size, Puzzle::Difficulty difficulty, int level);
    void randomLevel();
    void openRules();

private slots:
    void on_buttonPlay_clicked();
    void on_buttonRandom_clicked();
    void on_buttonRules_clicked();
    void on_buttonAbout_clicked();
    void on_buttonQuit_clicked();

public slots:
    void returnToPreviousMenu();
    void goToMainMenu();
    void goToDifficultyMenu(int size);
    void goToLevelMenu(int difficulty);
    void goToGame(int level);

private:
    Ui::ViewMenu *ui;
    int _size;
    Puzzle::Difficulty _difficulty;
    int _level;
    int _nbLevel;

};

#endif // VIEWMENU_H
