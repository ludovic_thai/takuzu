#ifndef VIEWGAMERULES_H
#define VIEWGAMERULES_H

#include <QMainWindow>

namespace Ui {
class ViewGameRules;
}

class ViewGameRules : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewGameRules(QWidget *parent = nullptr);
    ~ViewGameRules();

private slots:
    void on_pushButton_clicked();

    void on_actionClose_triggered();

private:
    Ui::ViewGameRules *ui;
};

#endif // VIEWGAMERULES_H
