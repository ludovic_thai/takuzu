#ifndef LEVELWIDGET_H
#define LEVELWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QButtonGroup>

class levelWidget : public QWidget
{
    Q_OBJECT
public:
    explicit levelWidget(QWidget *parent = nullptr);

    void setNbLevel(int nb);

private:
    QGridLayout* layout;
    QButtonGroup* buttonGroup;
    QWidget* _gridContainer;

signals:
    void levelChoose(int level);
    void returnToPreviousMenu();
};

#endif // LEVELWIDGET_H
