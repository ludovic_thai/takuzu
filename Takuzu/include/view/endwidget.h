#ifndef ENDWIDGET_H
#define ENDWIDGET_H

#include <QWidget>

namespace Ui {
class EndWidget;
}

class EndWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EndWidget(QWidget *parent = nullptr);
    ~EndWidget();
    void setEndMsg(QString mdg);

private slots:
    void on_returnToMenu_clicked();

    void on_replay_clicked();

    void on_nextPuzzle_clicked();

    void on_pushButton_clicked();

private:
    Ui::EndWidget *ui;

signals:
    void returnToMenu();
    void replay();
    void nextPuzzle();
    void quit();
};

#endif // ENDWIDGET_H
