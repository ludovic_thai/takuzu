#ifndef VIEWGAME_H
#define VIEWGAME_H

#include <QMainWindow>
#include "casewidget.h"
#include "puzzle.h"
#include <QElapsedTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class ViewGame; }
QT_END_NAMESPACE

class ViewGame : public QMainWindow
{
    Q_OBJECT


public:
    ViewGame(QWidget *parent = nullptr);
    ~ViewGame();

    void setCountWidgets();
    void reset();

public slots:
    /**
     * @brief Adds a case in the array contanning the game.
     * @param x
     * @param y
     * @param size of the puzzle (6, 8 or 10).
     * @param defaultValue
     */
    void addCase(int x, int y, int size, CaseWidget::Value defaultValue);

    /**
     * @brief Changes the value of a case in the array contanning the game.
     * @param row
     * @param col
     * @param value
     */
    void setValue(int row, int col, CaseWidget::Value value);

    /**
     * @brief Changes the validation of the a case.
     * @param x
     * @param y
     * @param validation of the case (0: NONE, 1: CORRECT, 2: INCORRECT).
     */
    void setValidation(int x, int y, int validation);

    /**
     * @brief Sets the label containning the difficulty of the game.
     * @param difficulty of the Game.
     */
    void setDifficultyLabel(Puzzle::Difficulty difficulty);

    /**
     * @brief Sets the label containning the size of the game.
     * @param size of the game, can be 6x6, 8x8 or 10x10.
     */
    void setSizeLabel(int size);

    /**
     * @brief Sets label containning the puzzle number.
     * @param puzzleNumber number of the puzzle.
     * @param total of puzzle in this difficulty and size.
     */
    void setPuzzleNumberLabel(int puzzleNumber, int total);

    /**
     * @brief Changes the values of number of black and white in the row.
     * @param x
     * @param blackCount
     * @param whiteCout
     */
    void setColorCountRow(int x, int blackCount, int whiteCout);

    /**
     * @brief Changes the values of number of black and white in the column.
     * @param x
     * @param blackCount
     * @param whiteCout
     */
    void setColorCountColumn(int x, int blackCount, int whiteCout);

    /**
     * @brief Hides or shows the CountWidget of the row.
     * @param hide
     * @param row
     */
    void toggleColorCountRow(bool hide, int row);

    /**
     * @brief Hides or shows the CountWidget of the column.
     * @param hide
     * @param row
     */
    void toggleColorCountCol(bool hide, int col);

    void startTimer();

    /**
     * @brief Stops the timer and shows the end screen.
     */
    void endGame();

    /**
     * @brief Sets the custom end message.
     * @param nbUndo
     * @param time
     */
    void setEndMsg(int nbUndo, qint64 time);

    /**
     * @brief Quits the application.
     */
    void quit();

    /**
     * @brief Changes the display of the CaseWidgets.
     * @param display (CIRCLE, SQUARE, BINARY, TICTACTOE)
     */
    void setDisplay(CaseWidget::Display display);

    void displayCircle();

    void displaySquare();

    void displayBinary();

    void displayTicTacToe();

signals:
    void updateCell(int x, int y, CaseWidget::Value oldValue, CaseWidget::Value newValue);

    void undo();

    void redo();

    void resetPuzzle();

    void elapsedTime(qint64 elapsedTime);

    void goToMenu();

    void replay();

    void nextPuzzle();

    void openRules();


private slots:
    void on_undoButton_clicked();

    void on_redoButton_clicked();

    void on_resetGameButton_clicked();

    void showTime();

    void returnToMenu();

    void replayPuzzle();

    void goToNextPuzzle();

    void on_pause_clicked();

    void on_actionReset_Game_triggered();

    void on_actionExit_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

    void on_actionRules_triggered();

    void on_actionAbout_triggered();

private:
    Ui::ViewGame *ui;
    int _size = 0;
    CaseWidget::Display _display = CaseWidget::CIRCLE;
    QElapsedTimer _timer;
    QTimer * tick;
    qint64 _pauseTime = 0;

    QString timeQint64ToQString(qint64 time);

};
#endif
