#ifndef COUNTWIDGET_H
#define COUNTWIDGET_H

#include <QWidget>


class CountWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CountWidget(QWidget *parent = nullptr);

    void setNb(int nbWhite, int nbBlack);

    void hide(bool hide);

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    int _nbWhite = 0;
    int _nbBlack = 0;
    bool _hide = false;

signals:

};

#endif // COUNTWIDGET_H
