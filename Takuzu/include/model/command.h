#ifndef COMMAND_H
#define COMMAND_H

#include <QObject>

class Command : public QObject
{
    Q_OBJECT
public:
    explicit Command(QObject *parent = nullptr);

    /**
     * @brief Cancels the current command
     */
    virtual void undo() = 0;

    /**
     * @brief Remakes the current command
     */
    virtual void redo() = 0;
signals:

};

#endif // COMMAND_H
