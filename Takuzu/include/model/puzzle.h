#ifndef PUZZLE_H
#define PUZZLE_H

#include <QFileSelector>
#include <QFile>
#include <iostream>

class Puzzle
{
public:
    enum Difficulty {
        EASY,
        HARD
    };

    enum Value {
        EMPTY,
        BLACK,
        WHITE
    };

    enum ValidationValue {
        NONE,
        CORRECT,
        INCORRECT
    };

    Puzzle(Difficulty difficulty, int size, int num, std::string puzzle);

    /**
     * @brief Checks the entire puzzle.
     * Updates _validationPuzzle.
     * @return
     */
    bool checkPuzzle();

    /**
     * @brief Updates the given cell (x, y) with a new value in the currentPuzzle.
     * Also updates the validationPuzle;
     * @param x
     * @param y
     * @param value
     */
    void updateCell(int x, int y, Value value);

    /**
     * @brief greenRow
     * @param row
     * @return True if the given row is correct (green), False otherwise.
     */
    bool greenRow(int row);

    /**
     * @brief greenColumn
     * @param column
     * @return True if the given column is correct (green), False otherwise.
     */
    bool greenColumn(int column);

    int ** getValidationPuzzle();
    int ** getCurrentPuzzle();
    int getSize();
    Difficulty getDifficulty();
    int getNum();
    int getBlackOnRow(int row);
    int getWhiteOnRow(int row);
    int getBlackOnColumn(int column);
    int getWhiteOnColumn(int column);
    bool getValidPuzzle();

    void printPuzzle();

private:
    Difficulty _difficulty;
    int _size;
    int _num;
    bool _validPuzzle;

    std::string _initialPuzzle;

    int ** _currentPuzzle;

    /**
     * @brief _size*3 array
     * for each row: AllRowGreen:0=FALSE/1=TRUE | numberOfBlack | numberOfWhite
     */
    int ** _rowInformations;

    /**
     * @brief _size*3 array
     * for each column: AllColGreen:0=FALSE/1=TRUE | numberOfBlack | numberOfWhite
     */
    int ** _columnInformations;

    /**
     * @brief Array which tells if a cell is false(0=2); N/A(0)
     * For the columns only
     */
    int ** _validationPuzzleColumn;

    /**
     * @brief Array which tells if a cell is false(0=2); N/A(0)
     * For the rows only
     */
    int ** _validationPuzzleRow;

    /**
     * @brief Array which tells if a cell is correct(1); false(0=2); N/A(0)
     * Take account all the rules
     */
    int ** _validationPuzzle;

    void createCurrentPuzzle();
    void initializeInformationsArray();

    /**
     * @brief Compares if two given rows are equal.
     * @brief Two Rows can be equal ONLY if they don't have empty cell.
     * @param firstRow
     * @param secondRow
     * @return TRUE if the two rows have the same cells, FALSE otherwise.
     */
    bool compareTwoRows(int firstRow, int secondRow);

    /**
     * @brief Compares if two given columns are equal.
     * @brief Two columns can be equal ONLY if they don't have empty cell.
     * @param firstColumn
     * @param secondColumn
     * @return TRUE if the two columns have the same cells, FALSE otherwise.
     */
    bool compareTwoColumns(int firstColumn, int secondColumn);

    /**
     * @brief Checks one row.
     * Updates _validationPuzzleRow and _rowInformations.
     * @param row
     * @return CORRECT the row is correct individually, INCORRECT otherwise.
     */
    ValidationValue checkOneRow(int row);

    /**
     * @brief Checks one column.
     * Updates _validationPuzzleColumn and _columnInformations.
     * @param column
     * @return CORRECT the column is correct individually, INCORRECT otherwise.
     */
    ValidationValue checkOneColumn(int column);

    /**
     * @brief Compares all rows between each other.
     * Updates _validationPuzzleRow.
     * @return TRUE if all the rows are different; FALSE otherwise.
     */
    bool checkAllRowsDifferent();

    /**
     * @brief Compares all columns between each other.
     * Updates _validationPuzzleColumn.
     * @return TRUE if all the colum are different; FALSE otherwise.
     */
    bool checkAllColumnsDifferent();

};

#endif // PUZZLE_H
