#ifndef COMMANDCLICK_H
#define COMMANDCLICK_H

#include "command.h"
#include "casewidget.h"

class CommandClick: public Command
{
    Q_OBJECT
private:
    int _row;
    int _col;
    CaseWidget::Value _oldValue;
    CaseWidget::Value _newValue;

public:
    /**
     * @brief CommandClick
     * @param row : The row of the CaseWidget
     * @param col : The Column of the CaseWidget
     * @param oldValue : The value before the click
     * @param newValue : The value after the click
     */
    CommandClick(int row, int col, CaseWidget::Value oldValue, CaseWidget::Value newValue);

    /**
     * @brief executes the command
     */
    void execute();

    void undo() override;

    void redo() override;

signals:
    void setValue(int row, int col, CaseWidget::Value value);

    void updateCell(int row, int col, CaseWidget::Value value);
};

#endif // COMMANDCLICK_H
