#ifndef PUZZLEMANAGER_H
#define PUZZLEMANAGER_H

#include <QString>
#include "puzzle.h"
#include <string>
#include <iostream>

class PuzzleManager
{
public:
    PuzzleManager();

    ~PuzzleManager();

    /**
     * @brief Gets the total number of a category of Puzzle.
     * @param difficulty
     * @param size
     * @return Number of puzzle in this specific category (defined by its difficulty and size).
     */
    static int getTotalPuzzles(Puzzle::Difficulty difficulty, int size);

    /**
     * @brief Gets the puzzle defined by the given properties.
     * @param difficulty
     * @param size
     * @param num
     * @return Puzzle.
     */
    static Puzzle* getPuzzle(Puzzle::Difficulty difficulty, int size, int num);
};

#endif // PUZZLEMANAGER_H
