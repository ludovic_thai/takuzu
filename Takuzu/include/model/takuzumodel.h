#ifndef TAKUZUMODEL_H
#define TAKUZUMODEL_H

#include "puzzle.h"
#include "casewidget.h"
#include "QObject"
#include "puzzlemanager.h"
#include <stack>
#include <commandclick.h>

class TakuzuModel: public QObject
{
    Q_OBJECT

public:
    TakuzuModel();

    /**
     * @brief Loads the puzzle in the model.
     * @param difficulty
     * @param size
     * @param num
     */
    void loadPuzzle(Puzzle::Difficulty difficulty, int size, int num);

    /**
     * @brief Sends a array telling for each cell if it's non/correct/incorrect
     * using signals.
     */
    void sendValidationPuzzle();

    /**
     * @brief Sends the colors of blacks and whites for the given column and row
     * using signals.
     * @param row
     * @param column
     */
    void sendColorCount(int row, int column);

    /**
     * @brief Send the initial puzzle cells using signals.
     */
    void sendInitialPuzzle();

    /**
     * @brief Send the initial color count for each
     */
    void sendInitialColorCount();

    qint64 getTotalGameTime();
    int getNumberOfUndo();


public slots:
    void updateCell(int row, int column, CaseWidget::Value oldValue, CaseWidget::Value newValue);
    void updateCell(int row, int col, CaseWidget::Value value);
    void undo();
    void redo();
    void reset();
    void addToTotalGameTime(qint64 gameTime);
    void randomLevel();
    void replay();
    void nextPuzzle();

signals:
    /**
     * @brief Signal to add a case in the view by emitting signal.
     * @param x
     * @param y
     * @param size of the puzzle (6, 8 or 10).
     * @param defaultValue
     */
    void addCase(int x, int y, int size, CaseWidget::Value defaultValue);

    /**
     * @brief Sets value of the cell (row, column).
     * @param row
     * @param col
     * @param value 0: EMPTY / 1: BLACK / 2: WHITE.
     */
    void setValue(int row, int col, CaseWidget::Value value);

    /**
     * @brief Signal to sets the label containning the difficulty of the game.
     * @param difficulty of the Game.
     */
    void setDifficultyLabel(Puzzle::Difficulty difficulty);

    /**
     * @brief Signal to sets the label containning the size of the game.
     * @param size of the game, can be 6x6, 8x8 or 10x10.
     */
    void setSizeLabel(int size);

    /**
     * @brief Signal to sets label containning the puzzle number.
     * @param puzzleNumber number of the puzzle.
     * @param total of puzzle in this difficulty and size.
     */
    void setPuzzleNumberLabel(int puzzleNumber, int total);

    /**
     * @brief Signal to set the validation value (color) of the cell (x,y)
     * @param x the row.
     * @param y the column.
     * @param value 0: NONE/ 1: CORRECT/ 2: INCORRECT.
     */
    void setValidation(int x, int y, int value);

    /**
     * @brief Signal to set the colors of the blacks and whites on a given row
     * @param row
     * @param blackCount number of black currently on the row
     * @param whiteCout number of white currently on the row
     */
    void setColorCountRow(int row, int blackCount, int whiteCout);

    /**
     * @brief Signal to set the colors of blacks and white on a given column
     * @param column
     * @param blackCount number of black currently on the column
     * @param whiteCout number of white currently on the column
     */
    void setColorCountColumn(int column, int blackCount, int whiteCout);

    /**
     * @brief Signal to tell if the given row is correct (green) or not.
     * @param value True for correct, False otherwise.
     * @param row
     */
    void correctRow(bool value, int row);

    /**
     * @brief Signal to tell if the given column is correct (green) or not.
     * @param value True for correct, False otherwise.
     * @param column
     */
    void correctColumn(bool value,int column);

    /**
     * @brief Signal to the timer (in the view) to start from 0.
     */
    void startTimer();

    /**
     * @brief Signal that the game is finished.
     */
    void endGame();

    /**
     * @brief Signal to go to game.
     */
    void goToGame(int, Puzzle::Difficulty, int);

    /**
     * @brief Signal to go to the menu.
     */
    void goToMenu();

    /**
     * @brief Signal to set the end message at the end of the game.
     * @param nbUndo Number of undo made by the player.
     * @param time to solve the puzzle.
     */
    void setEndMsg(int nbUndo, qint64 time);

private:
    Puzzle * _puzzle;
    int _numberOfUndo;
    qint64 _totalGameTime;
    std::stack<Command*>* commandsUndo = new std::stack<Command*>;
    std::stack<Command*>* commandsRedo = new std::stack<Command*>;
};

#endif // TAKUZUMODEL_H
